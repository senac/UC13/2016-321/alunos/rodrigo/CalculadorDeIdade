/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadordeidade;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeIdade {
      public static final String MAIOR_IDADE = "maior de Idade";
    public static final String MENOR_IDADE = "menor de Idade";
    public static final String IDOSO = "pessoa de Idosa";

    public String calcular(int idade) {

        if (idade > 65) {
            return IDOSO;
        } else if (idade >= 18) {
            return MAIOR_IDADE;
        } else {
            return MENOR_IDADE;
        }

    }
    
}
