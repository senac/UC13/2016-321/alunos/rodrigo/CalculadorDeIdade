/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadordeidade;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeIdadeTest {
    
    @Test
    public void deveSerMaiorDeIdade(){
        int idade = 21 ; 
        CalculadoraDeIdade calculadoraDeIdade = new CalculadoraDeIdade();
        String Resultado = calculadoraDeIdade.calcular(idade); 
        assertEquals(CalculadoraDeIdade.MAIOR_IDADE, Resultado);
        
    }
    
    
     @Test
    public void deveSerMenorDeIdade(){
        int idade = 17 ; 
        CalculadoraDeIdade calculadoraDeIdade = new CalculadoraDeIdade();
        String Resultado = calculadoraDeIdade.calcular(idade); 
        assertEquals(CalculadoraDeIdade.MENOR_IDADE, Resultado);
        
    }
    
     @Test
    public void deveSerIdoso(){
        int idade = 66 ; 
        CalculadoraDeIdade calculadoraDeIdade = new CalculadoraDeIdade();
        String Resultado = calculadoraDeIdade.calcular(idade); 
        assertEquals(CalculadoraDeIdade.IDOSO, Resultado);
        
    }
    
    
}
